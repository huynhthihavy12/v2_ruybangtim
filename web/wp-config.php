<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ruybangtim_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pK_6{uNi]Y5[^,Z,3Du*tb(Zq5)]aqlL{>RLLxWxf9qZZ7`cpEuArjdkomSy%Rk0');
define('SECURE_AUTH_KEY',  ':ml+!6%#%7973)GLy!RCEzu!9jq%2T`#ybmZDrx?s*9IFU>?}R5(rs[pw_Hcm@u*');
define('LOGGED_IN_KEY',    'ww}k%}B0w0inV$g`YzN1axMo~.o[xBvpLbAs(e7[|Q-B/X:h}ncjA )LpJWo&u,h');
define('NONCE_KEY',        '8UU_!9 ^(?*<Oz(CN3*/1pH[V4O]}=]%/5u0g0Nxf2w)Ww9=e2%!:wZS,yeT%#mP');
define('AUTH_SALT',        'R>)aaL, gl!JJRIz:~+aAL5U.r!9oT]u<kjkgi|_9._ud4{uXX,.7nCF&X,l 0nA');
define('SECURE_AUTH_SALT', '=C{.YXOemg`Du^$<_L4 ANMw21[L}4&|T3V>#q:W EBP$.-/%2(? 7vg/kp%$=s<');
define('LOGGED_IN_SALT',   '&8L$b(~c?}1g~bJq5xsZ}Au P1,``O_up!bU7 VsD&19t?)(},|zalmFh5Tb}=m ');
define('NONCE_SALT',       '+_g3$`8*wh;&s?Gc~pF@z^k/BWqHkj?Vqmo#8,,PKF]3>s{{^%(S:R;0_AsLbDh&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'rbt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
