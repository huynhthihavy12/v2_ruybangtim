<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage RuyBangTim
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<!-- main-container start -->
<!-- ================ -->
<section class="main-container light-gray-bg text-center margin-clear">

  <div class="container">
    <div class="row justify-content-lg-center">

      <!-- main start -->
      <!-- ================ -->
      <div class="main col-lg-6 pv-40">
        <h1 class="page-title extra-large"><span class="text-default">404</span></h1>
        <h2 class="mt-4">Ooops! Page Not Found</h2>
        <p class="lead">The requested URL was not found on this server. Make sure that the Web site address displayed
          in the address bar of your browser is spelled and formatted correctly.</p>
        <form role="search">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Search">
            <i class="fa fa-search form-control-feedback"></i>
          </div>
        </form>
        <a href="index.html" class="btn btn-default btn-animated btn-lg">Return Home <i class="fa fa-home"></i></a>
      </div>
      <!-- main end -->

    </div>
  </div>
</section>
<!-- main-container end -->

<?php get_footer();
