		<footer id="footer" class="clearfix">

			<!-- .footer start -->
			<!-- ================ -->
			<div class="footer default-bg text-white">
				<div class="container">
					<div class="footer-inner">
					<div class="row">
						<div class="col-12">
						<div class="footer-content">
							<ul class="social-links large circle text-center mt-5 mb-3">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-xing"></i></a></li>
							</ul>
							<p class="text-center"><span class="logo-font pr-10">Ruy Băng Tím</span> Copyright © 2018 Ruy Băng
							Tím.
							All Rights Reserved</p>
						</div>
						</div>
					</div>
					</div>
				</div>
			</div>
			<!-- .footer end -->
		</footer>
			<!-- footer end -->
		<?php wp_footer(); ?>
	</div>
</body>

</html>
