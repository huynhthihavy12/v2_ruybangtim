<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ruybangtim_pro
 */

?>

<div class="banner banner-page banner-no-img bg-gradient slideshow">
	<div class="container">
		<div class="row justify-content-lg-center text-center">
		<h1 class="w-100 title logo-font object-non-visible f-t-b text-white pl-2 pr-2" data-animation-effect="fadeIn"
			data-effect-delay="100"><?php __('PAGE NOT FOUND', 'ruybangtim-pro') ?></h1>
		</div>
	</div>
</div>
<div class="main-container fixed-bg border-clear bg-white text-center margin-clear">
	<div class="container">
		<div class="row justify-content-lg-center">
		<!-- main start -->
		<!-- ================ -->
		<div class="main col-lg-6 pv-40">
			<h1 class="page-title extra-large"><span class="text-default">404</span></h1>
			<h2 class="mt-4"><?php __('Ooops! Page Not Found', 'ruybangtim-pro') ?></h2>
			<p class="lead"><?php __('The requested URL was not found on this server. Make sure that the Web site address displayed in the address bar of your browser is spelled and formatted correctly.', 'ruybangtim-pro') ?></p>
			<!-- <form role="search">
			<div class="form-group has-feedback">
				<input type="text" class="form-control" placeholder="Search">
				<i class="fa fa-search form-control-feedback"></i>
			</div>
			</form> -->
			<a href="<?php echo get_home_url(); ?>" class="btn btn-default btn-animated btn-lg"><?php __('Return Home', 'ruybangtim-pro') ?><i class="fa fa-home"></i></a>
		</div>
		<!-- main end -->
		</div>
	</div>
</div>
