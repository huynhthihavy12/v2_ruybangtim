<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage RuyBangTim
 * @since 1.0
 * @version 1.2
 */

?>
<?php
	class Menu_Custom extends Walker_Nav_Menu
	{
		public function start_lvl(&$output, $depth = 0, $args = array())
		{
		if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = str_repeat($t, $depth);

		$classes = array('dropdown-menu');

		$class_names = join(' ', apply_filters('nav_menu_submenu_css_class', $classes, $args, $depth));
		$class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';
		$output .= "{$n}{$indent}<ul$class_names>" . "<span class='top-line'></span>" . "{$n}";
		}

		public function end_lvl(&$output, $depth = 0, $args = array())
		{
		if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = str_repeat($t, $depth);
		$output .= "$indent</ul>{$n}";
		}

		public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
		{
		$class_li = '';
		

		if ($args->walker->has_children) {
			$class_li = 'nav-item dropdown ';
			$class_a = $depth ? 'class="text-center"' : 'class="nav-link dropdown-toggle text-uppercase font-weight-bold pr-lg-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"';
		}else {
			$class_a = $depth ? 'class="text-center"' : 'class="nav-link text-uppercase font-weight-bold pr-lg-0"';
		}

		if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = ($depth) ? str_repeat($t, $depth) : '';

		$classes = empty($item->classes) ? array() : (array)$item->classes;
		$classes[] = '';

		$args = apply_filters('nav_menu_item_args', $args, $item, $depth);

		$class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args, $depth));
		$class_names = $class_names ? ' class="' . $class_li . esc_attr($class_names) . '"' : '';

		$id = apply_filters('nav_menu_item_id', '', $item, $args, $depth);
		$id = $id ? ' id="' . esc_attr($id) . '"' : '';

		$output .= $indent . '<li' . $id . $class_names . '>';

		$atts = array();
		$atts['title'] = !empty($item->attr_title) ? $item->attr_title : '';
		$atts['target'] = !empty($item->target) ? $item->target : '';
		$atts['rel'] = !empty($item->xfn) ? $item->xfn : '';
		$atts['href'] = !empty($item->url) ? $item->url : '';


		$atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args, $depth);

		$attributes = '';
		foreach ($atts as $attr => $value) {
			if (!empty($value)) {
			$value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
			$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}


		$title = apply_filters('the_title', $item->title, $item->ID);
		$title = apply_filters('nav_menu_item_title', $title, $item, $args, $depth);

		$item_output = $args->before;
		$item_output .= '<a ' . $class_a . $attributes . '>';
		$item_output .= $args->link_before . $title . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
		}

		public function end_el(&$output, $item, $depth = 0, $args = array())
		{
		if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$output .= "</li>{$n}";
		}
	}
?>

<nav class="navbar navbar-expand-lg navbar-light p-0">
	<div class="navbar-brand clearfix hidden-lg-up">
		<!-- logo -->
		<div id="logo-mobile" class="logo">
			<a href="index.html"><img id="logo-img-mobile" src="<?php echo get_template_directory_uri()?>/assets/images/logo_purple.png" width="80px" alt="Ruy băng tím"></a>
		</div>
	</div>

	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-1"
		aria-controls="navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<?php if ( has_nav_menu( 'top' ) ) :
		wp_nav_menu( array(
			'theme_location' 	=> 'top',
			'menu_id'        	=> 'top-menu',
			'container_class' 	=> 'collapse navbar-collapse',
			'container_id' 		=> 'navbar-collapse-1',
			'menu_class'        => 'navbar-nav ml-xl-auto',
			'walker' 			=> new Menu_Custom()
		) ); 
		endif; ?>
	<!-- <ul class="navbar-nav ml-xl-auto">
		<li class="nav-item dropdown">
			<a href="/" class="nav-link dropdown-toggle text-uppercase font-weight-bold pr-lg-0" id="first-dropdown"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">KIẾN THỨC UNG THƯ</a>
			<ul class="dropdown-menu" aria-labelledby="first-dropdown">
			<li><a href="/">EX</a></li>
			<li><a href="/">EX</a></li>
			<li><a href="/">EX</a></li>
			<li><a href="/">EX</a></li>
			<li><a href="/">EX</a></li>
			</ul>
		</li>
		<li class="nav-item dropdown">
			<a href="/" class="nav-link dropdown-toggle text-uppercase font-weight-bold pr-lg-0" id="first-dropdown"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ĐIỀU TRỊ</a>
			<ul class="dropdown-menu" aria-labelledby="first-dropdown">
			<li><a href="/">EX</a></li>
			</ul>
		</li>
		<li class="nav-item dropdown">
			<a href="/" class="nav-link dropdown-toggle text-uppercase font-weight-bold pr-lg-0" id="first-dropdown"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PHÒNG BỆNH</a>
			<ul class="dropdown-menu" aria-labelledby="first-dropdown">
			<li><a href="/">EX</a></li>
			</ul>
		</li>
		<li class="nav-item dropdown">
			<a href="/" class="nav-link dropdown-toggle text-uppercase font-weight-bold pr-lg-0" id="first-dropdown"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">TIN ĐỒN & SỰ THẬT</a>
			<ul class="dropdown-menu" aria-labelledby="first-dropdown">
			<li><a href="/">EX</a></li>
			</ul>
		</li>
		<li class="nav-item dropdown">
			<a href="/" class="nav-link dropdown-toggle text-uppercase font-weight-bold pr-lg-0" id="first-dropdown"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">GỐC CHUYÊN GIA</a>
			<ul class="dropdown-menu" aria-labelledby="first-dropdown">
			<li><a href="/">EX</a></li>
			</ul>
		</li>
		<li class="nav-item dropdown">
			<a href="/" class="nav-link dropdown-toggle text-uppercase font-weight-bold pr-lg-0" id="first-dropdown"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SỰ KIỆN</a>
			<ul class="dropdown-menu" aria-labelledby="first-dropdown">
			<li><a href="/">EX</a></li>
			</ul>
		</li>
		<li class="nav-item dropdown">
			<a href="/" class="nav-link dropdown-toggle text-uppercase font-weight-bold pr-lg-0" id="first-dropdown"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">LIÊN HỆ</a>
			<ul class="dropdown-menu" aria-labelledby="first-dropdown">
			<li><a href="/">EX</a></li>
			</ul>
		</li>
	</ul> -->
	
</nav><!-- #site-navigation -->
