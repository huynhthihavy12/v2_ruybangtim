<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage RuyBangTim
 * @since 1.0
 * @version 1.0
 */
?>
<!-- sidebar start -->
<!-- ================ -->
<aside class="col-lg-4 col-xl-4 ml-xl-auto">
	<div class="sidebar">
		<?php $sponsor = get_field('rbt_donor_list',20); ?>
		<?php if(!empty($sponsor)): ?>
		<div class="block clearfix">
			<h3 class="title">Nhà tài trợ</h3>
			<div class="separator-2"></div>
			<?php foreach($sponsor as $value) :?>
			<?php $image = $value['image'] ? $value['image'] : get_template_directory_uri()."/assets/images/portfolio-2.jpg"; ?>
			<div class="image-box shadow-2 text-center mb-20">
				<div class="overlay-container">
					<img src="<?php echo $image ?>" alt="<?php echo $value['name'] ?>">
					<div class="overlay-top">
						<div class="text">
							<h3><a href="<?php echo $value['website'] ?>"><?php echo $value['name'] ?></a></h3>
							<!-- <p class="small">App Development</p> -->
						</div>
					</div>
					<div class="overlay-bottom">
						<div class="links">
							<a href="<?php echo $value['website'] ?>" class="btn btn-gray-transparent btn-animated btn-sm">Chi tiết <i class="pl-10 fa fa-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach ?>
		</div>
		<?php endif ?>
		<?php 
		$popularpost = new WP_Query( array(
			'posts_per_page' => 4,
			'meta_key' => 'postview_number',
			'orderby' => 'meta_value_num',
			'order' => 'DESC',
		)); ?>
		
		<?php if($popularpost->have_posts()): ?>
		
		<div class="block clearfix">
			<h3 class="title">Xem nhiều nhất</h3>
			<div class="separator-2"></div>
			<?php while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
			<?php 
				$image_post = get_the_post_thumbnail_url($post->ID, 'medium') ;
				$post_link = get_permalink($post->id);
				$category_post = get_category($post->ID);
			?>
			<div class="media margin-clear">
				<div class="d-flex pr-2">
					<div class="overlay-container">
						<img class="media-object" src="<?php echo $image_post ? $image_post : get_template_directory_uri().'/assets/images/portfolio-1.jpg'?>" alt="<?php the_title() ?>">
						<a href="<?php echo $post_link ?>" class="overlay-link small"><i class="fa fa-link"></i></a>
					</div>
				</div>
				<div class="media-body">
					<h6 class="media-heading"><a href="<?php echo $post_link ?>"><?php the_title() ?></a></h6>
					<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i><?php echo get_the_date() ?></p>
				</div>
			</div>
			<hr>
			<?php endwhile; ?>
			<div class="text-right space-top">
				<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="link-dark"><i class="fa fa-plus-circle pl-1 pr-1"></i>Xem thêm</a>
			</div>
			
		</div>
		<?php wp_reset_query(); ?>
		<?php endif ?>
	</div>
</aside>
<!-- sidebar end -->