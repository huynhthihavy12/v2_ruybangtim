<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage RuyBangTim
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- Mobile Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800,800i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body class="front-page ">

  <!-- scrollToTop -->
  <!-- ================ -->
  <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

  <!-- page wrapper start -->
  <!-- ================ -->
  <div class="page-wrapper">
    <!-- header-container start -->
    <div class="header-container">
      <!-- header-top start -->
      <?php get_template_part( 'template-parts/header/top', 'header' ); ?>
      <!-- header-top end -->

      <!-- header start -->
      <?php get_template_part( 'template-parts/header/main', 'header' ); ?>
      <!-- header end -->

    </div>
    <!-- header-container end -->

