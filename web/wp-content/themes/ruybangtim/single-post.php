<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post();?>
<?php postview_set(get_the_ID()); ?>
<?php 
    $image_post = get_the_post_thumbnail_url($post->ID, 'full') ;
    $post_link = get_permalink($post->id);
?>
<!-- header-container end -->
<!-- breadcrumb start -->
<!-- ================ -->
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a href="index.html">Trang chủ</a></li>
            <li class="breadcrumb-item active"><?php the_title() ?></li>
        </ol>
    </div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">
    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-lg-8">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title"><?php the_title() ?></h1>
                <!-- page-title end -->

                <!-- blogpost start -->
                <!-- ================ -->
                <article class="blogpost full">
                    <header>
                        <div class="post-info mb-4">
                            <span class="post-date">
                                <i class="fa fa-calendar-o pr-1"></i>
                                <span class="day"><?php echo get_the_date() ?></span>
                            </span>
                            <!-- <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John Doe</a></span>
                            <span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22 comments</a></span> -->
                        </div>
                    </header>
                    <div class="blogpost-content">
                        <div class="mb-5">
                            <div class="overlay-container">
                                <img src="<?php echo $image_post ? $image_post : get_template_directory_uri().'/assets/images/blog-1.jpg' ?>" alt="<?php the_title() ?>">
                            </div>
                        </div>
                        <?php the_content(); ?>
                    </div>
                    <footer class="clearfix">
                        <!-- <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a href="#">tag
                                2</a>, <a href="#">long tag 3</a></div> -->
                        <div class="link pull-right">
                            <ul class="social-links circle small colored clearfix margin-clear text-right animated-effect-1">
                                <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div>
                    </footer>
                </article>
                <!-- blogpost end -->

            </div>
            <!-- main end -->

            <!-- sidebar start -->
            <!-- ================ -->
            <?php get_template_part( 'sidebar' ); ?>
            <!-- sidebar end -->

        </div>
    </div>
</section>
<!-- main-container end -->


<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>