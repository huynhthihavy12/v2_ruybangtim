<?php

/**
 * Template Name: Blog Template
 * Display page for the theme.
**/ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post();?>

<!-- breadcrumb start -->
<!-- ================ -->
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a href="index.html">Trang chủ</a></li>
            <li class="breadcrumb-item active">Blog Masonry Left Sidebar</li>
        </ol>
    </div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-lg-8">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title">Blog Masonry Left Sidebar</h1>
                <div class="separator-2"></div>
                <!-- page-title end -->

                <!-- masonry grid start -->
                <!-- ================ -->
                <div class="masonry-grid row">
                    <!-- masonry grid item start -->
                    <div class="masonry-grid-item col-md-6">
                        <!-- blogpost start -->
                        <article class="blogpost shadow-2 light-gray-bg bordered">
                            <div class="overlay-container">
                                <img src="images/blog-1.jpg" alt="">
                                <a class="overlay-link" href="blog-post.html"><i class="fa fa-link"></i></a>
                            </div>
                            <header>
                                <h2><a href="blog-post.html">Blogpost with slider</a></h2>
                                <div class="post-info">
                                    <span class="post-date">
                                        <i class="fa fa-calendar-o pr-1"></i>
                                        <span class="day">12</span>
                                        <span class="month">May 2017</span>
                                    </span>
                                    <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John
                                            Doe</a></span>
                                    <span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22
                                            comments</a></span>
                                </div>
                            </header>
                            <div class="blogpost-content">
                                <p>Mauris dolor sapien, malesuada at interdum ut, hendrerit eget lorem. Nunc interdum
                                    mi neque, et sollicitudin purus fermentum ut. Suspendisse faucibus nibh odio, a
                                    vehicula eros pharetra in. Maecenas ullamcorper commodo rutrum. In iaculis lectus
                                    vel augue eleifend dignissim. Aenean viverra semper sollicitudin.</p>
                            </div>
                            <footer class="clearfix">
                                <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a
                                        href="#">tag 2</a>, <a href="#">long tag 3</a></div>
                                <div class="link pull-right"><i class="fa fa-link pr-1"></i><a href="#">Read More</a></div>
                            </footer>
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- masonry grid item end -->

                    <!-- masonry grid item start -->
                    <div class="masonry-grid-item col-md-6">
                        <!-- blogpost start -->
                        <article class="blogpost shadow-2 light-gray-bg bordered">

                            <header>
                                <h2><a href="blog-post.html">Lorem upsoum dolor is</a></h2>
                                <div class="post-info">
                                    <span class="post-date">
                                        <i class="fa fa-calendar-o pr-1"></i>
                                        <span class="day">10</span>
                                        <span class="month">May 2017</span>
                                    </span>
                                    <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John
                                            Doe</a></span>
                                    <span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22
                                            comments</a></span>
                                </div>
                            </header>
                            <div class="blogpost-content">
                                <p>Mauris dolor sapien, malesuada at interdum ut, hendrerit eget lorem. Nunc interdum
                                    mi neque, et sollicitudin purus fermentum ut. Suspendisse faucibus nibh odio, a
                                    vehicula eros pharetra in. Maecenas ullamcorper commodo rutrum. In iaculis lectus
                                    vel augue eleifend dignissim. Aenean viverra semper sollicitudin.</p>
                            </div>
                            <footer class="clearfix">
                                <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a
                                        href="#">tag 2</a>, <a href="#">long tag 3</a></div>
                                <div class="link pull-right"><i class="fa fa-link pr-1"></i><a href="#">Read More</a></div>
                            </footer>
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- masonry grid item end -->

                    <!-- masonry grid item start -->
                    <div class="masonry-grid-item col-md-6">
                        <!-- blogpost start -->
                        <article class="blogpost shadow-2 light-gray-bg bordered object-non-visible"
                            data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                            <header>
                                <h2><a href="blog-post.html">Text Post</a></h2>
                                <div class="post-info">
                                    <span class="post-date">
                                        <i class="fa fa-calendar-o pr-1"></i>
                                        <span class="day">09</span>
                                        <span class="month">May 2017</span>
                                    </span>
                                    <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John
                                            Doe</a></span>
                                    <span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22
                                            comments</a></span>
                                </div>
                            </header>
                            <div class="blogpost-content">
                                <p>Mauris dolor sapien, malesuada at interdum ut, hendrerit eget lorem. Nunc interdum
                                    mi neque, et sollicitudin purus fermentum ut. Suspendisse faucibus nibh odio, a
                                    vehicula eros pharetra in. Maecenas ullamcorper commodo rutrum. In iaculis lectus
                                    vel augue eleifend dignissim. Aenean viverra semper sollicitudin.</p>
                            </div>
                            <footer class="clearfix">
                                <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a
                                        href="#">tag 2</a>, <a href="#">long tag 3</a></div>
                                <div class="link pull-right"><i class="fa fa-link pr-1"></i><a href="#">Read More</a></div>
                            </footer>
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- masonry grid item end -->

                    <!-- masonry grid item start -->
                    <div class="masonry-grid-item col-md-6">
                        <!-- blogpost start -->
                        <article class="blogpost shadow-2 light-gray-bg bordered object-non-visible"
                            data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                            <div class="overlay-container">
                                <img src="images/page-about-2.jpg" alt="">
                                <a class="overlay-link" href="blog-post.html"><i class="fa fa-link"></i></a>
                            </div>
                            <header>
                                <h2><a href="blog-post.html">Excepteur sint occaecat cupidatat non</a></h2>
                                <div class="post-info">
                                    <span class="post-date">
                                        <i class="fa fa-calendar-o pr-1"></i>
                                        <span class="day">08</span>
                                        <span class="month">May 2017</span>
                                    </span>
                                    <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John
                                            Doe</a></span>
                                    <span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22
                                            comments</a></span>
                                </div>
                            </header>
                            <div class="blogpost-content">
                                <p>Mauris dolor sapien, malesuada at interdum ut, hendrerit eget lorem. Nunc interdum
                                    mi neque, et sollicitudin purus fermentum ut. Suspendisse faucibus nibh odio, a
                                    vehicula eros pharetra in. Maecenas ullamcorper commodo rutrum. In iaculis lectus
                                    vel augue eleifend dignissim. Aenean viverra semper sollicitudin.</p>
                            </div>
                            <footer class="clearfix">
                                <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a
                                        href="#">tag 2</a>, <a href="#">long tag 3</a></div>
                                <div class="link pull-right"><i class="fa fa-link pr-1"></i><a href="#">Read More</a></div>
                            </footer>
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- masonry grid item end -->

                    <!-- masonry grid item start -->
                    <div class="masonry-grid-item col-md-6">
                        <!-- blogpost start -->
                        <article class="blogpost shadow-2 light-gray-bg bordered object-non-visible"
                            data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                            <div class="overlay-container">
                                <img src="images/blog-2.jpg" alt="">
                                <a class="overlay-link" href="blog-post.html"><i class="fa fa-link"></i></a>
                            </div>
                            <header>
                                <h2><a href="blog-post.html">Cute Robot</a></h2>
                                <div class="post-info">
                                    <span class="post-date">
                                        <i class="fa fa-calendar-o pr-1"></i>
                                        <span class="day">08</span>
                                        <span class="month">May 2017</span>
                                    </span>
                                    <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John
                                            Doe</a></span>
                                    <span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22
                                            comments</a></span>
                                </div>
                            </header>
                            <div class="blogpost-content">
                                <p>Mauris dolor sapien, malesuada at interdum ut, hendrerit eget lorem. Nunc interdum
                                    mi neque, et sollicitudin purus fermentum ut. Suspendisse faucibus nibh odio, a
                                    vehicula eros pharetra in. Maecenas ullamcorper commodo rutrum. In iaculis lectus
                                    vel augue eleifend dignissim. Aenean viverra semper sollicitudin.</p>
                            </div>
                            <footer class="clearfix">
                                <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a
                                        href="#">tag 2</a>, <a href="#">long tag 3</a></div>
                                <div class="link pull-right"><i class="fa fa-link pr-1"></i><a href="#">Read More</a></div>
                            </footer>
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- masonry grid item end -->

                    <!-- masonry grid item start -->
                    <div class="masonry-grid-item col-md-6">
                        <!-- blogpost start -->
                        <article class="blogpost shadow-2 light-gray-bg bordered object-non-visible"
                            data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                            <div class="overlay-container">
                                <img src="images/blog-3.jpg" alt="">
                                <a class="overlay-link" href="blog-post.html"><i class="fa fa-link"></i></a>
                            </div>
                            <header>
                                <h2><a href="blog-post.html">Nunc interdum mi neque</a></h2>
                                <div class="post-info">
                                    <span class="post-date">
                                        <i class="fa fa-calendar-o pr-1"></i>
                                        <span class="day">07</span>
                                        <span class="month">May 2017</span>
                                    </span>
                                    <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John
                                            Doe</a></span>
                                    <span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22
                                            comments</a></span>
                                </div>
                            </header>
                            <div class="blogpost-content">
                                <p>Mauris dolor sapien, malesuada at interdum ut, hendrerit eget lorem. Nunc interdum
                                    mi neque, et sollicitudin purus fermentum ut. Suspendisse faucibus nibh odio, a
                                    vehicula eros pharetra in. Maecenas ullamcorper commodo rutrum. In iaculis lectus
                                    vel augue eleifend dignissim. Aenean viverra semper sollicitudin.</p>
                            </div>
                            <footer class="clearfix">
                                <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a
                                        href="#">tag 2</a>, <a href="#">long tag 3</a></div>
                                <div class="link pull-right"><i class="fa fa-link pr-1"></i><a href="#">Read More</a></div>
                            </footer>
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- masonry grid item end -->

                    <!-- masonry grid item start -->
                    <div class="masonry-grid-item col-md-6">
                        <!-- blogpost start -->
                        <article class="blogpost shadow-2 light-gray-bg bordered object-non-visible"
                            data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                            <div class="overlay-container">
                                <img src="images/blog-4.jpg" alt="">
                                <a class="overlay-link" href="blog-post.html"><i class="fa fa-link"></i></a>
                            </div>
                            <header>
                                <h2><a href="blog-post.html">Distinctio magni aut illum</a></h2>
                                <div class="post-info">
                                    <span class="post-date">
                                        <i class="fa fa-calendar-o pr-1"></i>
                                        <span class="day">06</span>
                                        <span class="month">May 2017</span>
                                    </span>
                                    <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John
                                            Doe</a></span>
                                    <span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22
                                            comments</a></span>
                                </div>
                            </header>
                            <div class="blogpost-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio magni aut illum
                                    animi laboriosam esse quidem sit illo fugit est culpa facilis amet, ab doloremque
                                    magnam, sapiente architecto natus. Voluptatem esse optio explicabo dolore impedit
                                    debitis deleniti porro doloribus provident perferendis nostrum quasi nobis magni
                                    atque mollitia, quas, quos corporis!</p>
                            </div>
                            <footer class="clearfix">
                                <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a
                                        href="#">tag 2</a>, <a href="#">long tag 3</a></div>
                                <div class="link pull-right"><i class="fa fa-link pr-1"></i><a href="#">Read More</a></div>
                            </footer>
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- masonry grid item end -->

                    <!-- masonry grid item start -->
                    <div class="masonry-grid-item col-md-6">
                        <!-- blogpost start -->
                        <article class="blogpost shadow-2 light-gray-bg bordered object-non-visible"
                            data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                            <header>
                                <h2><a href="blog-post.html">Voluptatem esse optio explicabo dolore impedit</a></h2>
                                <div class="post-info">
                                    <span class="post-date">
                                        <i class="fa fa-calendar-o pr-1"></i>
                                        <span class="day">06</span>
                                        <span class="month">May 2017</span>
                                    </span>
                                    <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John
                                            Doe</a></span>
                                    <span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22
                                            comments</a></span>
                                </div>
                            </header>
                            <div class="blogpost-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio magni aut illum
                                    animi laboriosam esse quidem sit illo fugit est culpa facilis amet, ab doloremque
                                    magnam, sapiente architecto natus. Voluptatem esse optio explicabo dolore impedit
                                    debitis deleniti porro doloribus provident perferendis nostrum quasi nobis magni
                                    atque mollitia, quas, quos corporis!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto veniam,
                                    perspiciatis libero quasi, iusto delectus sed accusantium fugit inventore iure
                                    molestiae amet voluptatem, ut dolorem ex dolorum ea impedit quae!</p>
                            </div>
                            <footer class="clearfix">
                                <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a
                                        href="#">tag 2</a>, <a href="#">long tag 3</a></div>
                                <div class="link pull-right"><i class="fa fa-link pr-1"></i><a href="#">Read More</a></div>
                            </footer>
                        </article>
                        <!-- blogpost end -->
                    </div>
                    <!-- masonry grid item end -->
                </div>
                <!-- masonry grid end -->

                <!-- pagination start -->
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <i aria-hidden="true" class="fa fa-angle-left"></i>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <i aria-hidden="true" class="fa fa-angle-right"></i>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- pagination end -->
            </div>
            <!-- main end -->

            <!-- sidebar start -->
            <!-- ================ -->
            <?php get_template_part( 'sidebar' ); ?>
            <!-- sidebar end -->

        </div>
    </div>
</section>
<!-- main-container end -->

<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer() ?>
