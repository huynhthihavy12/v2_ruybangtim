<?php
/**
 * Ruy Băng Tím back compat functionality
 *
 * Prevents Ruy Băng Tím from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package WordPress
 * @subpackage RuyBangTim
 * @since Ruy Băng Tím 1.0
 */

/**
 * Prevent switching to Ruy Băng Tím on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Ruy Băng Tím 1.0
 */
function ruybangtim_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'ruybangtim_upgrade_notice' );
}
add_action( 'after_switch_theme', 'ruybangtim_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Ruy Băng Tím on WordPress versions prior to 4.7.
 *
 * @since Ruy Băng Tím 1.0
 *
 * @global string $wp_version WordPress version.
 */
function ruybangtim_upgrade_notice() {
	$message = sprintf( __( 'Ruy Băng Tím requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'ruybangtim' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since Ruy Băng Tím 1.0
 *
 * @global string $wp_version WordPress version.
 */
function ruybangtim_customize() {
	wp_die( sprintf( __( 'Ruy Băng Tím requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'ruybangtim' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'ruybangtim_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since Ruy Băng Tím 1.0
 *
 * @global string $wp_version WordPress version.
 */
function ruybangtim_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Ruy Băng Tím requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'ruybangtim' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'ruybangtim_preview' );
