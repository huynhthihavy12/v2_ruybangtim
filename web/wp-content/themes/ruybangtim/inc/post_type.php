<?php
// Our custom post type function
add_action( 'init', 'position_taxonomy');
function position_taxonomy() {
    register_taxonomy(
        'position_taxonomy',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'position',  //post type name
        array(
        'public'                => true,
        'hierarchical'          => true,
        'label'                 => __( 'Position', 'ruybangtim' ),  //Display name
        'query_var'             => true,
        'show_admin_column' => true,
        'rewrite'               => array(
            'slug'              => 'position-category', // This controls the base slug that will display before each term
            'with_front'        => false // Don't display the category base before
            )
        )
    );
}

function create_posttype() {

    register_post_type( 'position',
        array(
            'labels' => array(
                'name' => __( 'Tác giả','ruybangtim' ),
                'singular_name' => __( 'Tác giả', 'ruybangtim' ),
                'add_new'               => __( 'Add','ruybangtim' ),
                'all_items'             => __( 'All','ruybangtim' ),
                'view_item'             => __( 'View', 'ruybangtim' ),
                'edit_item'             => __( 'Edit', 'ruybangtim' ),
            ),
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'excerpt'
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'position'),
            'show_in_nav_menus' => true,
            'can_export' => true,
            'taxonomies'=> array( 'position_taxonomy' ),
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );
