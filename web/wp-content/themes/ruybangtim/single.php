<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post();?>

<!-- header-container end -->
<!-- breadcrumb start -->
<!-- ================ -->
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a href="index.html">Trang chủ</a></li>
            <li class="breadcrumb-item active">Blog Post</li>
        </ol>
    </div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">
    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-lg-8">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title">Blog Post</h1>
                <!-- page-title end -->

                <!-- blogpost start -->
                <!-- ================ -->
                <article class="blogpost full">
                    <header>
                        <div class="post-info mb-4">
                            <span class="post-date">
                                <i class="fa fa-calendar-o pr-1"></i>
                                <span class="day">12</span>
                                <span class="month">May 2017</span>
                            </span>
                            <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John Doe</a></span>
                            <span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22 comments</a></span>
                        </div>
                    </header>
                    <div class="blogpost-content">
                        <div id="carousel-blog-post" class="carousel slide mb-5" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-blog-post" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-blog-post" data-slide-to="1"></li>
                                <li data-target="#carousel-blog-post" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <div class="overlay-container">
                                        <img src="images/blog-1.jpg" alt="">
                                        <a class="overlay-link popup-img" href="images/blog-1.jpg"><i class="fa fa-search-plus"></i></a>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="overlay-container">
                                        <img src="images/blog-3.jpg" alt="">
                                        <a class="overlay-link popup-img" href="images/blog-3.jpg"><i class="fa fa-search-plus"></i></a>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="overlay-container">
                                        <img src="images/blog-4.jpg" alt="">
                                        <a class="overlay-link popup-img" href="images/blog-4.jpg"><i class="fa fa-search-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 class="my-4">Corporis ullam nemo itaque excepturi suscipit</h3>
                        <p class="large">Cumque cum quos, rem, quibusdam autem ullam nihil suscipit! Atque provident
                            sed sapiente velit cupiditate nisi placeat alias enim, aspernatur voluptate, dolorem.</p>
                        <p>Mauris dolor sapien, <a href="#">malesuada at interdum ut</a>, hendrerit eget lorem. Nunc
                            interdum mi neque, et sollicitudin purus fermentum ut. Suspendisse faucibus nibh odio, a
                            vehicula eros pharetra in. Maecenas ullamcorper commodo rutrum. In iaculis lectus vel augue
                            eleifend dignissim. Aenean viverra semper sollicitudin.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ullam nemo itaque
                            excepturi suscipit unde repudiandae nesciunt ad voluptates minima recusandae illum
                            exercitationem, neque, ut totam ratione. Consequuntur consequatur ad nesciunt nulla
                            voluptate voluptates qui natus labore facilis dolore odit vero ea sint inventore tenetur et
                            eligendi nobis fugit veniam quod possimus, quasi, voluptatem. Cupiditate?</p>
                        <ol>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, laboriosam tempore,
                                veniam repudiandae dolor aperiam, iste porro amet odio eius earum tempora? Ex nobis
                                suscipit, nam in eius, deserunt nihil.</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis odit est quae amet
                                iure quia reiciendis maxime eos blanditiis tenetur voluptates, ab, obcaecati eaque
                                accusamus dolorem a beatae mollitia quod?</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam aperiam vel cum quisquam
                                enim reprehenderit sunt cupiditate ullam, id quidem perspiciatis dolore molestiae iure
                                odio. Dolore fuga voluptate, deleniti placeat.</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, eaque. Totam nisi
                                ducimus dolor ab obcaecati temporibus asperiores, ad dignissimos dolorum unde fuga quae
                                voluptates beatae quasi voluptatum culpa dolore?</li>
                        </ol>
                        <blockquote>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                        </blockquote>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus incidunt, beatae rem omnis
                            distinctio, dolor, praesentium impedit quisquam nobis pariatur nulla expedita aliquid
                            repellendus laudantium. A illum sint corrupti eligendi quae ab, facilis eos quas! Velit
                            earum facere ex maxime.</p>
                    </div>
                    <footer class="clearfix">
                        <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a href="#">tag
                                2</a>, <a href="#">long tag 3</a></div>
                        <div class="link pull-right">
                            <ul class="social-links circle small colored clearfix margin-clear text-right animated-effect-1">
                                <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div>
                    </footer>
                </article>
                <!-- blogpost end -->

                <!-- comments start -->
                <!-- ================ -->
                <div id="comments" class="comments">
                    <h2 class="title">There are 3 comments</h2>

                    <!-- comment start -->
                    <div class="comment clearfix">
                        <div class="comment-avatar">
                            <img class="rounded-circle" src="images/avatar.jpg" alt="avatar">
                        </div>
                        <header>
                            <h3>Comment title</h3>
                            <div class="comment-meta">By <a href="#">admin</a> | Today, 12:31</div>
                        </header>
                        <div class="comment-content">
                            <div class="comment-body clearfix">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
                                <a href="blog-post.html" class="btn-sm-link link-dark pull-right"><i class="fa fa-reply"></i>
                                    Reply</a>
                            </div>
                        </div>

                        <!-- comment start -->
                        <div class="comment clearfix">
                            <div class="comment-avatar">
                                <img class="rounded-circle" src="images/avatar.jpg" alt="avatar">
                            </div>
                            <header>
                                <h3>Comment title</h3>
                                <div class="comment-meta">By <a href="#">admin</a> | Today, 12:31</div>
                            </header>
                            <div class="comment-content">
                                <div class="comment-body clearfix">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
                                    <a href="blog-post.html" class="btn-sm-link link-dark pull-right"><i class="fa fa-reply"></i>
                                        Reply</a>
                                </div>
                            </div>
                        </div>
                        <!-- comment end -->

                    </div>
                    <!-- comment end -->

                    <!-- comment start -->
                    <div class="comment clearfix">
                        <div class="comment-avatar">
                            <img class="rounded-circle" src="images/avatar.jpg" alt="avatar">
                        </div>
                        <header>
                            <h3>Comment title</h3>
                            <div class="comment-meta">By <a href="#">admin</a> | Today, 12:31</div>
                        </header>
                        <div class="comment-content">
                            <div class="comment-body clearfix">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo </p>
                                <a href="blog-post.html" class="btn-sm-link link-dark pull-right"><i class="fa fa-reply"></i>
                                    Reply</a>
                            </div>
                        </div>
                    </div>
                    <!-- comment end -->

                </div>
                <!-- comments end -->

                <!-- comments form start -->
                <!-- ================ -->
                <div class="comments-form">
                    <h2 class="title">Add your comment</h2>
                    <form>
                        <div class="form-group has-feedback">
                            <label for="name4">Name</label>
                            <input type="text" class="form-control" id="name4" placeholder="" required>
                            <i class="fa fa-user form-control-feedback"></i>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="subject4">Subject</label>
                            <input type="text" class="form-control" id="subject4" placeholder="" required>
                            <i class="fa fa-pencil form-control-feedback"></i>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="message4">Message</label>
                            <textarea class="form-control" rows="8" id="message4" placeholder="" required></textarea>
                            <i class="fa fa-envelope-o form-control-feedback"></i>
                        </div>
                        <input type="submit" value="Submit" class="btn btn-default">
                    </form>
                </div>
                <!-- comments form end -->

            </div>
            <!-- main end -->

            <!-- sidebar start -->
            <!-- ================ -->
            <?php get_template_part( 'sidebar' ); ?>
            <!-- sidebar end -->

        </div>
    </div>
</section>
<!-- main-container end -->


<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>