<?php get_header(); ?>
<?php if ( have_posts() ) : ?>


<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

	<div class="container">
		<div class="row">

			<!-- main start -->
			<!-- ================ -->
			<div class="main col-lg-8">

				<!-- page-title start -->
				<!-- ================ -->
				<h1 class="page-title">Blog Masonry Left Sidebar</h1>
				<div class="separator-2"></div>
				<!-- page-title end -->

				<!-- masonry grid start -->
				<!-- ================ -->
				<div class="masonry-grid row">
					<?php while ( have_posts() ) : the_post();?>
					<!-- masonry grid item start -->
					<div class="masonry-grid-item col-md-6">
						<!-- blogpost start -->
						<article class="blogpost shadow-2 light-gray-bg bordered">
							<div class="overlay-container">
								<img src="images/blog-1.jpg" alt="">
								<a class="overlay-link" href="blog-post.html"><i class="fa fa-link"></i></a>
							</div>
							<header>
								<h2><a href="blog-post.html">Blogpost with slider</a></h2>
								<div class="post-info">
									<span class="post-date">
										<i class="fa fa-calendar-o pr-1"></i>
										<span class="day">12</span>
										<span class="month">May 2017</span>
									</span>
									<span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John
											Doe</a></span>
									<span class="comments"><i class="fa fa-comments-o pl-1 pr-1"></i> <a href="#">22
											comments</a></span>
								</div>
							</header>
							<div class="blogpost-content">
								<p>Mauris dolor sapien, malesuada at interdum ut, hendrerit eget lorem. Nunc interdum
									mi neque, et sollicitudin purus fermentum ut. Suspendisse faucibus nibh odio, a
									vehicula eros pharetra in. Maecenas ullamcorper commodo rutrum. In iaculis lectus
									vel augue eleifend dignissim. Aenean viverra semper sollicitudin.</p>
							</div>
							<footer class="clearfix">
								<div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a href="#">tag 2</a>, <a
									 href="#">long tag 3</a></div>
								<div class="link pull-right"><i class="fa fa-link pr-1"></i><a href="#">Read More</a></div>
							</footer>
						</article>
						<!-- blogpost end -->
					</div>
					<!-- masonry grid item end -->
					<?php endwhile; ?>
				</div>
				<!-- masonry grid end -->

				<!-- pagination start -->
				<nav aria-label="Page navigation">
					<ul class="pagination justify-content-center">
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Previous">
								<i aria-hidden="true" class="fa fa-angle-left"></i>
								<span class="sr-only">Previous</span>
							</a>
						</li>
						<li class="page-item active"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">4</a></li>
						<li class="page-item"><a class="page-link" href="#">5</a></li>
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Next">
								<i aria-hidden="true" class="fa fa-angle-right"></i>
								<span class="sr-only">Next</span>
							</a>
						</li>
					</ul>
				</nav>
				<!-- pagination end -->
			</div>
			<!-- main end -->

			<!-- sidebar start -->
			<!-- ================ -->
			<?php get_template_part( 'sidebar' ); ?>
			<!-- sidebar end -->

		</div>
	</div>
</section>
<!-- main-container end -->
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer() ?>