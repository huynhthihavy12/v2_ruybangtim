<?php
/**
 * Template for displaying search forms in Ruy Băng Tím
 *
 * @package WordPress
 * @subpackage RuyBangTim
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( ruybangtim_unique_id( 'search-form-' ) ); ?>
