<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post();?>

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title">Jane Doe</h1>
                <div class="separator-2"></div>
                <!-- page-title end -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="image-box team-member shadow mb-20">
                            <div class="overlay-container overlay-visible">
                                <img src="images/team-member-3.jpg" alt="">
                                <a href="images/team-member-3.jpg" class="popup-img overlay-link" title="Jane Doe - CEO"><i
                                        class="fa fa-plus"></i></a>
                                <div class="overlay-bottom">
                                    <div class="text">
                                        <h3 class="title margin-clear">Jane Doe</h3>
                                        <p class="margin-clear">CTO</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-5">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus nam, vitae autem quis,
                            deserunt pariatur! At, atque inventore.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam esse laudantium maiores
                            aperiam illo fugit laboriosam velit repellendus quod cumque ea vero vitae quo enim fugiat
                            itaque harum assumenda aut quis, dolore. Sit reiciendis eligendi, recusandae eaque est
                            optio reprehenderit!</p>
                        <div class="progress style-2 dark">
                            <span class="text"></span>
                            <div class="progress-bar progress-bar-white" role="progressbar" data-animate-width="95%">
                                <span class="label object-non-visible" data-animation-effect="fadeInLeftSmall"
                                    data-effect-delay="1000">CSS</span>
                            </div>
                        </div>
                        <div class="progress style-2 dark">
                            <span class="text"></span>
                            <div class="progress-bar progress-bar-white" role="progressbar" data-animate-width="85%">
                                <span class="label object-non-visible" data-animation-effect="fadeInLeftSmall"
                                    data-effect-delay="1000">HTML5</span>
                            </div>
                        </div>
                        <div class="progress style-2 dark">
                            <span class="text"></span>
                            <div class="progress-bar progress-bar-white" role="progressbar" data-animate-width="95%">
                                <span class="label object-non-visible" data-animation-effect="fadeInLeftSmall"
                                    data-effect-delay="1000">Design</span>
                            </div>
                        </div>
                        <div class="progress style-2 dark">
                            <span class="text"></span>
                            <div class="progress-bar progress-bar-white" role="progressbar" data-animate-width="80%">
                                <span class="label object-non-visible" data-animation-effect="fadeInLeftSmall"
                                    data-effect-delay="1000">PHP</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ml-xl-auto">
                        <h3 class="title">Contact Me</h3>
                        <ul class="list-icons">
                            <li><i class="fa fa-phone pr-10 text-default"></i> +00 1234567890</li>
                            <li><i class="fa fa-mobile pr-10 text-default"></i> +00 1234567890</li>
                            <li><a href="mailto:info@janedoe.com"><i class="fa fa-envelope-o pr-10"></i>info@janedoe.com</a></li>
                        </ul>
                        <h3 class="mt-4">Follow Me</h3>
                        <div class="separator-2"></div>
                        <a href="#" class="btn btn-animated linkedin btn-sm">Linkedin<i class="pl-10 fa fa-linkedin"></i></a>
                        <a href="#" class="btn btn-animated xing btn-sm">Xing<i class="fa fa-xing"></i></a>
                        <h3 class="mt-4">See My Portfolio</h3>
                        <a class="btn btn-gray collapsed btn-animated" data-toggle="collapse" href="#collapseContent"
                            aria-expanded="false" aria-controls="collapseContent">Click Me <i class="fa fa-plus"></i></a>
                    </div>
                </div>

            </div>
            <!-- main end -->

        </div>
    </div>
</section>
<!-- main-container end -->

<!-- section start -->
<!-- ================ -->
<section id="collapseContent" class="collapse pv-20 light-gray-bg clearfix">
    <div class="container">
        <h3 class="mt-4">Latest <strong>Projects</strong></h3>
        <div class="separator-2 mb-20"></div>
        <div class="image-box style-3-b">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="overlay-container">
                        <img src="images/portfolio-1.jpg" alt="">
                        <div class="overlay-to-top">
                            <p class="small margin-clear"><em>Some info <br> Lorem ipsum dolor sit</em></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-9">
                    <div class="body">
                        <h3 class="title">Project Title</h3>
                        <p class="small mb-10"><i class="fa fa-calendar-o pr-1"></i> Feb, 2018 <i class="pl-10 pr-1 fa fa-tag"></i>
                            Web Design</p>
                        <div class="separator-2"></div>
                        <p class="mb-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam atque ipsam
                            nihialal. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam laudantium,
                            provident culpa saepe.</p>
                        <a href="#" class="btn btn-default-transparent btn-sm btn-animation btn-animation--slide-horizontal margin-clear">Read
                            More<i class="fa fa-arrow-right pl-10"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="image-box style-3-b">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="overlay-container">
                        <img src="images/portfolio-2.jpg" alt="">
                        <div class="overlay-to-top">
                            <p class="small margin-clear"><em>Some info <br> Lorem ipsum dolor sit</em></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-9">
                    <div class="body">
                        <h3 class="title">Project Title</h3>
                        <p class="small mb-10"><i class="fa fa-calendar-o pr-1"></i> Feb, 2018 <i class="pl-10 pr-1 fa fa-tag"></i>
                            Web Design</p>
                        <div class="separator-2"></div>
                        <p class="mb-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam atque ipsam
                            nihialal. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam laudantium,
                            provident culpa saepe.</p>
                        <a href="#" class="btn btn-default-transparent btn-sm btn-animation btn-animation--slide-horizontal margin-clear">Read
                            More<i class="fa fa-arrow-right pl-10"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="image-box style-3-b">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="overlay-container">
                        <img src="images/portfolio-3.jpg" alt="">
                        <div class="overlay-to-top">
                            <p class="small margin-clear"><em>Some info <br> Lorem ipsum dolor sit</em></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-9">
                    <div class="body">
                        <h3 class="title">Project Title</h3>
                        <p class="small mb-10"><i class="fa fa-calendar-o pr-1"></i> Feb, 2018 <i class="pl-10 pr-1 fa fa-tag"></i>
                            Web Design</p>
                        <div class="separator-2"></div>
                        <p class="mb-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam atque ipsam
                            nihialal. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam laudantium,
                            provident culpa saepe.</p>
                        <a href="#" class="btn btn-default-transparent btn-sm btn-animation btn-animation--slide-horizontal margin-clear">Read
                            More<i class="fa fa-arrow-right pl-10"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- pagination start -->
        <nav aria-label="Page navigation">
            <ul class="pagination justify-content-center">
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <i aria-hidden="true" class="fa fa-angle-left"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">5</a></li>
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <i aria-hidden="true" class="fa fa-angle-right"></i>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- pagination end -->
    </div>
</section>
<!-- section end -->

<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>