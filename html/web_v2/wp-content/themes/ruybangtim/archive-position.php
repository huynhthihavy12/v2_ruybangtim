<?php get_header(); ?>
<?php if ( have_posts() ) : ?>

<?php $obj = get_post_type_object( 'position' ); ?>

<!-- breadcrumb start -->
<!-- ================ -->
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="/">Trang chủ</a></li>
            <li class="breadcrumb-item active"><?php echo $obj->labels->singular_name ?></li>
        </ol>
    </div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-md-12">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title"><?php echo $obj->labels->singular_name ?></h1>
                <div class="separator-2"></div>
                <!-- page-title end -->
                <?php while ( have_posts() ) : the_post();?>
                <?php
                  $image_post = get_the_post_thumbnail_url($post->ID, 'medium') ;
                  $post_link = get_permalink($post->id);
                  $category_post = get_category($post->ID);
                ?>
                <div class="image-box team-member style-3-b">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xl-3">
                            <div class="overlay-container overlay-visible">
                                <img src="<?php echo $image_post ? $image_post : get_template_directory_uri().'/assets/images/team-member-1.jpg'?>" alt="<?php the_title() ?>">
                                <a href="<?php echo $post_link ?>" class="popup-img overlay-link" title="<?php the_title() ?>"><i
                                        class="fa fa-plus"></i></a>
                                <!-- <div class="overlay-bottom hidden-sm-down">
                                    <div class="text">
                                        <p class="small margin-clear"><em>Some info <br> Lorem ipsum dolor sit</em></p>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-6">
                            <div class="body">
                                <h3 class="title margin-clear"><?php the_title() ?></h3>
                                <div class="separator-2 mt-10"></div>
                                <p><?php the_excerpt() ?></p>
                                <ul class="social-links circle margin-clear colored">
                                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                                <!-- <br>
                                <a href="#" class="btn btn-gray-transparent"><i class="pr-10 fa margin-clear fa-phone"></i>Gọi</a>
                                <a href="mailto:youremail@domain.com" class="btn btn-gray-transparent"><i class="pr-10 margin-clear fa fa-envelope-o"></i>Liên hệ</a> -->
                            </div>
                        </div>
                        <!-- <div class="col-md-12 col-lg-4 col-xl-3 mb-4 mb-md-0">
                            <div class="pv-20 hidden-md-down"></div>
                            <div class="progress style-1 dark">
                                <span class="text"></span>
                                <div class="progress-bar progress-bar-white" role="progressbar" data-animate-width="95%">
                                    <span class="label object-non-visible" data-animation-effect="fadeInLeftSmall"
                                        data-effect-delay="1000">CSS</span>
                                </div>
                            </div>
                            <div class="progress style-1 dark">
                                <span class="text"></span>
                                <div class="progress-bar progress-bar-white" role="progressbar" data-animate-width="85%">
                                    <span class="label object-non-visible" data-animation-effect="fadeInLeftSmall"
                                        data-effect-delay="1000">HTML5</span>
                                </div>
                            </div>
                            <div class="progress style-1 dark">
                                <span class="text"></span>
                                <div class="progress-bar progress-bar-white" role="progressbar" data-animate-width="95%">
                                    <span class="label object-non-visible" data-animation-effect="fadeInLeftSmall"
                                        data-effect-delay="1000">Design</span>
                                </div>
                            </div>
                            <div class="progress style-1 dark">
                                <span class="text"></span>
                                <div class="progress-bar progress-bar-white" data-animate-width="80%">
                                    <span class="label object-non-visible" data-animation-effect="fadeInLeftSmall"
                                        data-effect-delay="1000">PHP</span>
                                </div>
                            </div>
                            <div class="progress style-1 dark">
                                <span class="text"></span>
                                <div class="progress-bar progress-bar-white" data-animate-width="85%">
                                    <span class="label object-non-visible" data-animation-effect="fadeInLeftSmall"
                                        data-effect-delay="1000">jQuery</span>
                                </div>
                            </div>
                            <div class="progress style-1 dark">
                                <span class="text"></span>
                                <div class="progress-bar progress-bar-white" data-animate-width="90%">
                                    <span class="label object-non-visible" data-animation-effect="fadeInLeftSmall"
                                        data-effect-delay="1000">Drupal</span>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                <?php endwhile; ?>
                <?php get_custom_pagination() ?>
            </div>
            <!-- main end -->

        </div>
    </div>
</section>
<!-- main-container end -->
<?php wp_reset_query(); ?>
<?php endif; ?>

<?php get_footer() ?>