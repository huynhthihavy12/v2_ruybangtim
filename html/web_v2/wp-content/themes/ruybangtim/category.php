<?php get_header(); ?>
<?php if ( have_posts() ) : ?>


<!-- breadcrumb start -->
<!-- ================ -->
<div class="breadcrumb-container">
	<div class="container">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a href="/">Trang chủ</a></li>
			<li class="breadcrumb-item active"><?php echo single_term_title() ?></li>
		</ol>
	</div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

	<div class="container">
		<div class="row">

			<!-- main start -->
			<!-- ================ -->
			<div class="main col-lg-8">

				<!-- page-title start -->
				<!-- ================ -->
				<h1 class="page-title font-weight-bold"><?php echo single_term_title() ?></h1>
				<div class="separator-2"></div>
				<!-- page-title end -->

				<?php while ( have_posts() ) : the_post();?>
				<?php 
					$image_post = get_the_post_thumbnail_url($post->ID, 'medium') ;
					$post_link = get_permalink($post->id);
					$category_post = get_category($post->ID);
				?>
				<!-- blogpost start -->
				<article class="blogpost">
					<div class="row grid-space-10">
						<div class="col-lg-6">
							<div class="overlay-container">
								<img src="<?php echo $image_post ? $image_post : get_template_directory_uri().'/assets/images/portfolio-1.jpg'?>" alt="">
								<a class="overlay-link" href="<?php echo $post_link ?>"><i class="fa fa-link"></i></a>
							</div>
						</div>
						<div class="col-lg-6">
							<header>
								<h2><a href="<?php echo $post_link ?>"><?php the_title() ?></a></h2>
								<div class="post-info">
									<span class="post-date">
										<i class="fa fa-calendar-o pr-1"></i>
										<span class="day">09<?php echo get_the_date() ?></span>
									</span>
									<!-- <span class="submitted"><i class="fa fa-user pr-1 pl-1"></i> by <a href="#">John Doe</a></span> -->
								</div>
							</header>
							<div class="blogpost-content">
								<p><?php the_excerpt() ?></p>
							</div>
						</div>
					</div>
					<footer class="clearfix">
						<!-- <div class="tags pull-left"><i class="fa fa-tags pr-1"></i> <a href="#">tag 1</a>, <a href="#">tag 2</a>, <a href="#">long
								tag 3</a></div> -->
						<div class="link pull-right"><i class="fa fa-link pr-1"></i><a href="<?php echo $post_link ?>">Xem thêm</a></div>
					</footer>
				</article>
				<!-- blogpost end -->

				<?php endwhile; ?>
				<?php get_custom_pagination() ?>
			</div>
			<!-- main end -->
			
			<!-- sidebar start -->
			<!-- ================ -->
			<?php get_template_part( 'sidebar' ); ?>
			<!-- sidebar end -->

		</div>
	</div>
</section>
<!-- main-container end -->
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer() ?>