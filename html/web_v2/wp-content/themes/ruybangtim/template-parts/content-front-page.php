<?php

/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ruybangtim_pro
 */
$slider = get_field('rbt_homepage_slider');
?>

<!-- ================================================================================================== -->
    
    
    <!-- banner start -->
    <!-- ================ -->
    <?php if(!empty($slider)): ?>
    <div class="banner clearfix">

      <!-- slideshow start -->
      <!-- ================ -->
      <div class="slideshow">

        <!-- slider revolution start -->
        <!-- ================ -->
        <div class="slider-revolution-5-container">
          <div id="slider-banner-fullscreen" class="slider-banner-fullscreen rev_slider" data-version="5.0">
            <ul class="slides">
            <?php foreach ($slider as $key => $value) {?>          
              <!-- slide 1 start -->
              <!-- ================ -->
              <li data-transition="random-static" data-slotamount="default" data-masterspeed="default" data-title="<?php echo $value["title"] ? $value["title"] : 'RBT' ; ?>">
              <!-- main image -->
                <img src="<?php echo $value['image'] ?>" alt="slidebg1" data-bgposition="center center" data-bgrepeat="no-repeat"
                  data-bgfit="cover" class="rev-slidebg">

                <!-- Transparent Background -->
                <div class="tp-caption dark-translucent-bg" data-x="center" data-y="center" data-start="0"
                  data-transform_idle="o:1;" data-transform_in="o:0;s:600;e:Power2.easeInOut;" data-transform_out="o:0;s:600;"
                  data-width="5000" data-height="5000">
                </div>

                <!-- LAYER NR. 1 -->
                <?php if($value["title"]): ?>
                <div class="tp-caption large_white" data-x="<?php  echo $value["position"] ?>" data-y="200" data-start="500" data-transform_idle="o:1;"
                  data-transform_in="y:[100%];sX:1;sY:1;o:0;s:1150;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                  data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                  <span class="logo-font"><?php echo $value["title"] ?></span>
                </div>
                <?php endif ?>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption large_white tp-resizeme" data-x="<?php  echo $value["position"] ?>" data-y="270" data-start="750"
                  data-transform_idle="o:1;" data-transform_in="o:0;s:2000;e:Power4.easeInOut;">
                  <div class="separator-2 light"></div>
                </div>

                <!-- LAYER NR. 3 -->
                <?php if($value["description"]): ?>
                <div class="tp-caption medium_white" data-x="<?php  echo $value["position"] ?>" data-y="290" data-start="750" data-transform_idle="o:1;"
                  data-transform_in="y:[100%];sX:1;sY:1;s:850;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"><?php echo $value["description"] ?>
                </div>
                <?php endif ?>

                <!-- LAYER NR. 4 -->
                <?php if($value["button_name"]): ?>
                <div class="tp-caption small_white" data-x="<?php  echo $value["position"] ?>" data-y="410" data-start="1000" data-transform_idle="o:1;"
                  data-transform_in="y:[100%];sX:1;sY:1;o:0;s:600;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                  data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                  <a href="<?php echo $value['button_url'] ?>" class="btn btn-<?php  echo $value["button_color"] ?> btn-lg btn-animated"><?php echo $value["button_name"] ?> <i class="fa fa-info-circle"></i></a>
                </div>
                <?php endif ?>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption" data-x="center" data-y="bottom" data-voffset="100" data-start="1250"
                  data-transform_idle="o:1;" data-transform_in="y:[100%];sX:1;sY:1;o:0;s:2000;e:Power4.easeInOut;"
                  data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                  <a href="#page-start" class="btn btn-lg moving smooth-scroll"><i class="fa fa-angle-down"></i><i
                      class="fa fa-angle-down"></i><i class="fa fa-angle-down"></i></a>
                </div>
              </li>
              <!-- slide 1 end -->
              <?php 
             } ?>
            </ul>
            <div class="tp-bannertimer"></div>
          </div>
        </div>
        <!-- slider revolution end -->

      </div>
      <!-- slideshow end -->

    </div>
    <?php endif ?>
    <!-- banner end -->

    <?php  $cancer_list = get_categories(
            array( 
              'parent' => 2,
              'hide_empty'   => 0,
            )
    ); ?>
    <!-- section start -->
    <!-- ================ -->
    <?php if(!empty($cancer_list)) : ?>
    <section id="section1" class="section light-gray-bg pv-30 padding-bottom-clear clearfix">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-lg-12">
          <?php $category = get_category( 2 ); ?>
            <h2 class="text-center mt-4 mb-5 font-weight-bold text-uppercase text-primary"><?php echo $category->name ?></h2>
          </div>
        </div>
        <div class="row mb-5 d-flex flex-wrap">
        <?php foreach ($cancer_list as $key => $value) { ?>
          <div class="pr-3 w-20 mb-3">
            <div class="hc-item-box-2 hc-element-invisible animated hc-element-visible fadeInDownSmall"
              data-animation-effect="fadeInDownSmall" data-effect-delay="100">
              <div class="body d-flex align-items-center">
                <span class="icon without-bg"><i class="fa text-default fa-heart-o"></i></span>
                <a href=" <?php echo get_category_link( $value->term_id )  ?>" class="font-weight-bold text-uppercase"><?php echo $value->name ?></a>
              </div>
            </div>
          </div>
        <?php  } ?>
        </div>
      </div>
    </section>
    <!-- section end -->
    <?php endif ?>

    <?php
      query_posts(array(
        'post_type'		   => 'post',
        'orderby'          => 'date',
        'post_status'      => 'publish',
        'suppress_filters' => true,
        'posts_per_page' => 10,
        'posts_per_archive_page' => 10,
      ));
    ?>
    <?php if ( have_posts() ) : ?>
    <!-- main-container start -->
    <!-- ================ -->
    <section id="section2" class="main-container">

      <div class="container">
        <div class="row">

          <!-- main start -->
          <!-- ================ -->
          <div class="main col-lg-8">

            <!-- page-title start -->
            <!-- ================ -->
            <h1 class="page-title font-weight-bold text-uppercase">BÀI VIẾT</h1>
            <div class="separator-2"></div>
            <!-- page-title end -->
            <?php while (have_posts() ) : the_post();?>
            <?php 
                  $image_post = get_the_post_thumbnail_url($post->ID, 'medium') ;
                  $post_link = get_permalink($post->id);
                  $category_post = get_category($post->ID);
            ?>
            <div class="image-box style-3-b">
              <div class="row">
                <div class="col-md-6 col-lg-5">
                  <div class="overlay-container">
                    <img src="<?php echo $image_post ? $image_post : get_template_directory_uri().'/assets/images/portfolio-1.jpg'?>" alt="">
                    <!-- <div class="overlay-to-top">
                      <p class="small margin-clear"><em>Some info <br> Lorem ipsum dolor sit</em></p>
                    </div> -->
                  </div>
                </div>
                <div class="col-md-6 col-lg-7">
                  <div class="body">
                    <h3 class="title"><a href="<?php echo $post_link ?>"><?php the_title() ?></a></h3>
                    <p class="small mb-10"><i class="fa fa-calendar-o pr-1"></i> <?php echo get_the_date() ?> <?php echo $category_post ? '<i class="pl-10 pr-1 fa fa-tag"></i>'.$category_post->name : '' ?></p>
                    <div class="separator-2"></div>
                    <p class="mb-10"><?php the_excerpt() ?></p>
                    <a href="<?php echo $post_link ?>" class="btn btn-default-transparent btn-animated">Chi tiết<i class="fa fa-arrow-right pl-10"></i></a>
                  </div>
                </div>
              </div>
            </div>
            <?php endwhile ?>

            <div class="row">
              <div class="col-lg-12 text-center">
                <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="btn btn-lg radius-50 btn-default btn-animated text-white">Tất cả <i class="fa fa-arrow-right"></i></a>
              </div>
            </div>

          </div>
          <!-- main end -->

          <?php get_template_part( 'sidebar' ); ?>

        </div>
      </div>
    </section>
    <!-- main-container end -->
    <?php wp_reset_query(); ?>
    <?php endif ?>

    <?php $args = array(
        'orderby'           => 'date',
        'post_status'       => 'publish',
        'suppress_filters'  => true,
        'cat'            => 5
    );
    $the_query = new WP_Query($args); ?>
    <?php if ($the_query->have_posts()) : ?>
    <!-- section start -->
    <!-- ================ -->
    <section id="section3" class="dark-translucent-bg fixed-bg background-img-10 pv-40" style="background-position: 50% 50%;">
      <div class="row justify-content-md-center">
        <div class="col-lg-12">
          <?php $category1 = get_category( 5 ); ?>
          <h2 class="text-center mt-4 mb-5 font-weight-bold text-uppercase"><?php echo $category1->name ?></h2>
        </div>
      </div>
      <div class="slick-carousel carousel">
      <?php while ($the_query->have_posts() ) : $the_query->the_post();?>
      <?php $image = get_the_post_thumbnail_url($post->ID, 'medium') ; ?>
        <div>
          <div class="col-md-12">
            <div class="image-box style-2 mb-20">
              <div class="overlay-container overlay-visible">
                <img src="<?php echo $image ? $image : get_template_directory_uri().'/assets/images/agency-portfolio-1.jpg'?>" alt="">
                <a href="<?php get_permalink($post->ID) ?>" class="overlay-link"><i class="fa fa-link"></i></a>
                <div class="overlay-bottom">
                  <div class="text pl-0 pr-0">
                    <p class="lead margin-clear text-left mobile-visible f-s-14"><?php the_title() ?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile ?>
      </div>
      <div class="row">
        <div class="col-lg-12 text-center">
          <a href="<?php echo get_category_link(5) ?>" class="btn btn-lg radius-50 btn-default-transparent btn-animated text-white">Xem thêm <i class="fa fa-arrow-right"></i></a>
        </div>
      </div>
    </section>
    <!-- section end -->
    <?php wp_reset_query(); ?>
    <?php endif ?>


    <?php
      query_posts(array(
        'post_type'		   => 'position',
        'orderby'          => 'date',
        'post_status'      => 'publish',
        'suppress_filters' => true,
        'posts_per_page' => 10,
        'posts_per_archive_page' => 10,
      ));
    ?>
    <?php if ( have_posts() ) : ?>
    <!-- section start -->
    <!-- ================ -->
    <section class="bg-light py-5">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="display-5 font-weight-bold mb-5 text-uppercase">DANH SÁCH CHUYÊN GIA</h2>
          </div>
        </div>
        <div class="row">
          <div class="row col-md-8">
            <?php while (have_posts() ) : the_post();?>
            <?php
              $image = get_the_post_thumbnail_url($post->ID, 'medium');
              $position = get_the_terms($post->ID, 'position_taxonomy');
            ?>
            <div class="col-md-6 col-xl-6">
              <div class="image-box image-box--shadowed white-bg style-2 mb-4">
                <div class="overlay-container d-flex justify-content-center">
                  <img src="<?php echo $image ? $image : get_template_directory_uri().'/assets/images/agency-portfolio-1.jpg'?>" alt="">
                  <a href="#" class="overlay-link"></a>
                </div>
                <div class="body">
                  <h5 class="font-weight-bold my-2"><?php the_title() ?></h5>
                  <p class="small"><?php echo $position ? $position[0]->name : ''?></p>
                  <div class="row d-flex align-items-center">
                    <article class="col-md-12 text-justify f-s-14">
                    <?php the_excerpt() ?>
                    </article>
                  </div>
                  <div class="row d-flex align-items-center">
                    <div class="col-6">
                      <ul class="social-links small circle">
                        <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li class="instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                      </ul>
                    </div>
                    <div class="col-6 text-right">
                      <a href="<?php echo get_permalink($post->ID) ?>" class="btn radius-50 btn-default-transparent btn-animated">Chi tiết <i class="fa fa-arrow-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php endwhile ?>
            <div class="col-lg-12 text-center">
                <a href="<?php echo get_post_type_archive_link( 'position' ); ?>" class="btn btn-lg radius-50 btn-default btn-animated text-white">Xem thêm <i class="fa fa-arrow-right"></i></a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="row">
              <div class="col-12">
                <h4 class="font-weight-bold mb-3 text-default">HỎI & ĐÁP CÙNG CHUYÊN GIA</h4>
              </div>
            </div>
            <form class="margin-clear">
              <div class="form-group has-feedback">
                <label class="sr-only" for="name2">Name</label>
                <input type="text" class="form-control" id="name2" placeholder="Name" required="">
                <i class="fa fa-user form-control-feedback"></i>
              </div>
              <div class="form-group has-feedback">
                <label class="sr-only" for="email2">Email address</label>
                <input type="email" class="form-control" id="email2" placeholder="Enter email" required="">
                <i class="fa fa-envelope form-control-feedback"></i>
              </div>
              <div class="form-group has-feedback">
                <label class="sr-only" for="message2">Message</label>
                <textarea class="form-control" rows="6" id="message2" placeholder="Message" required=""></textarea>
                <i class="fa fa-pencil form-control-feedback"></i>
              </div>
              <button type="submit" class="btn btn-default-transparent btn-animated margin-clear">Send <i class="fa fa-send"></i></button>
            </form>
          </div>
        </div>

      </div>
    </section>
    <!-- section end -->
    <?php endif ?>