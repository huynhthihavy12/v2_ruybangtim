      <header class="header fixed fixed-desktop clearfix white-bg">
        <div class="col-md-12 pl-lg-3 pr-lg-3">
            <div class="d-flex align-items-center">
                <div class="hidden-md-down">
                    <div class="header-first clearfix pt-lg-0">

                        <!-- logo -->
                        <div id="logo" class="logo">
                            <?php
                                $image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' );
                            ?>
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img id="logo_img" src="<?php echo  isset($image[0]) ? $image[0] : get_template_directory_uri()?>/assets/images/logo_purple.png" alt="Ruy băng tím" width="110px"></a>
                        </div>
                    </div>
                    <!-- header-first end -->

                </div>


                <div class="header-second clearfix ml-lg-auto">
                    <div class="main-navigation main-navigation--mega-menu main-navigation--style-2 animated">
                        <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                    </div>
                </div>


                <div class="hidden-md-down pl-3">
                    <div class="header-dropdown-buttons p-0">
                        <div class="btn-group">
                            <button type="button" class="btn dropdown-toggle dropdown-toggle--no-caret" id="header-drop-1"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></button>
                            <ul class="dropdown-menu dropdown-menu-right dropdown-animation" aria-labelledby="header-drop-1">
                                <li>
                                    <form role="search" method="GET" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search-box margin-clear">
                                        <div class="form-group has-feedback">
                                            <input name="s" value="<?php echo get_search_query(); ?>" type="text" class="form-control" placeholder="Tìm kiếm">
                                            <i class="fa fa-search form-control-feedback"></i>
                                        </div>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-auto pl-0 hidden-md-down">
                    <!-- header dropdown buttons -->
                    <div class="header-dropdown-buttons pt-0">
                    <a href="<?php echo get_page_link(20) ?>" class="btn btn-default">Tài trợ</a>
                    <!-- <a href="#" class="btn btn-default-transparent btn-animated">Đăng
                        ký nhận tin <i class="fa fa-envelope" aria-hidden="true"></i></a> -->
                    </div>
                    <!-- header dropdown buttons end -->
                </div>
            </div>
        </div>
    </header>


