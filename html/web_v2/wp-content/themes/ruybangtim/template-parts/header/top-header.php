<div class="header-top colored">
    <div class="container">
        <div class="row">
            <div class="col-3 col-sm-6 col-lg-9">
                <!-- header-top-first start -->
                <!-- ================ -->
                <div class="header-top-first clearfix">
                <ul class="social-links circle small clearfix hidden-sm-down">
                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li class="skype"><a href="#"><i class="fa fa-skype"></i></a></li>
                    <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li class="googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    <li class="flickr"><a href="#"><i class="fa fa-flickr"></i></a></li>
                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                </ul>
                <div class="social-links hidden-md-up circle small">
                    <div class="btn-group dropdown">
                    <button id="header-top-drop-1" type="button" class="btn dropdown-toggle dropdown-toggle--no-caret"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share-alt"></i></button>
                    <ul class="dropdown-menu dropdown-animation" aria-labelledby="header-top-drop-1">
                        <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="skype"><a href="#"><i class="fa fa-skype"></i></a></li>
                        <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li class="googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                        <li class="flickr"><a href="#"><i class="fa fa-flickr"></i></a></li>
                        <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                    </ul>
                    </div>
                </div>
                <ul class="list-inline hidden-md-down">
                    <li class="list-inline-item"><i class="fa fa-map-marker pr-1 pl-2"></i>One Infinity Loop Av, Tk
                    123456</li>
                    <li class="list-inline-item"><i class="fa fa-phone pr-1 pl-2"></i>+12 123 123 123</li>
                    <li class="list-inline-item"><i class="fa fa-envelope-o pr-1 pl-2"></i> theproject@mail.com</li>
                </ul>
                </div>
                <!-- header-top-first end -->
            </div>
            <div class="col-9 col-sm-6 col-lg-3">

                <!-- header-top-second start -->
                <!-- ================ -->
                <div id="header-top-second" class="clearfix">
                </div>
                <!-- header-top-second end -->
            </div>
        </div>
    </div>
</div>

