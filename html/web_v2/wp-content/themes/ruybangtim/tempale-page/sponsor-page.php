<?php

/**
 * Template Name: Nhà tài trợ Template
 * Display page for the theme.
**/ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post();?>
<?php $sponsor = get_field('rbt_donor_list',$post->ID); ?>

<!-- breadcrumb start -->
<!-- ================ -->
<div class="breadcrumb-container">
	<div class="container">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a href="/">Trang chủ</a></li>
			<li class="breadcrumb-item active">
				<?php the_title() ?>
			</li>
		</ol>
	</div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<?php if(is_array($sponsor)): ?>
<section class="main-container">

	<div class="container">
		<div class="row">

			<!-- main start -->
			<!-- ================ -->
			<div class="main col-12">

				<!-- page-title start -->
				<!-- ================ -->
				<h1 class="page-title"><?php the_title() ?></h1>
				<div class="separator-2"></div>
				<!-- page-title end -->
				<p class="lead"><?php the_content() ?></p>

				<!-- isotope filters start -->
				<div class="filters">
					<ul class="nav nav-pills">
						<li class="nav-item"><a class="nav-link active" href="#" data-filter="*">All</a></li>
						<!-- <li class="nav-item"><a class="nav-link" href="#" data-filter=".web-design">Web design</a></li>
						<li class="nav-item"><a class="nav-link" href="#" data-filter=".app-development">App development</a></li>
						<li class="nav-item"><a class="nav-link" href="#" data-filter=".site-building">Site building</a></li> -->
					</ul>
				</div>
				<!-- isotope filters end -->

				<div class="isotope-container row grid-space-10">
					
					<?php foreach ($sponsor as $key => $value) { 
						$image = $value['image'] ? $value['image'] : get_template_directory_uri()."/assets/images/portfolio-2.jpg";
					?>
						<div class="col-lg-3 col-md-6 isotope-item app-development">
							<div class="image-box shadow-2 text-center mb-20">
								<div class="overlay-container">
									<img src="<?php echo $image ?>" alt="<?php echo $value['name'] ?>">
									<div class="overlay-top">
										<div class="text">
											<h3><a href="<?php echo $value['website'] ?>"><?php echo $value['name'] ?></a></h3>
											<!-- <p class="small">App Development</p> -->
										</div>
									</div>
									<div class="overlay-bottom">
										<div class="links">
											<a href="<?php echo $value['website'] ?>" class="btn btn-gray-transparent btn-animated btn-sm">Chi tiết <i class="pl-10 fa fa-arrow-right"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>

				</div>

			</div>
			<!-- main end -->

		</div>
	</div>
</section>
<?php endif ?>
<!-- main-container end -->


<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>